<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class NotebookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		$content = file_get_contents("./public/1.json");   //загружаю json-данные из файла
		$people = json_decode($content, true);            //json-данные записываю в массив
		foreach ($people as $person) : 
		$user = []; 
			foreach ($person as $key => $value) { 
				if ($key !== 'id') { 
					$user[$key] = $value;
				};
			}; 
		DB::table('notebook')->insert([	$user ]);
		endforeach; 

    	// DB::table('notebook')->insert([
    	// 	'first_name' => 'Фитискин', 
    	// 	'last_name' => 'Дмитрий', 
    	// 	'mobil_phone' => '', 
    	// 	'Adress' => 'Самарская обл., г.Самара', 
    	// 	'Organization' => 'Netology', 
    	// 	'Job' => '', 
    	// 	'Comments' => 'Куратор курсов по программированию в нетологии', 
    	// 	'Netology' => '', 
    	// 	'Contacts' => '', 
    	// 	'Email' => 'dfitiskin@gmail.com', 
    	// 	'vk' => '', 
    	// 	'fb' => 'https://www.facebook.com/dfitiskin' 
    	// 	]);
     //    dd(123);

    }
}
