<?php 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






Route::get( '/', 'UserController1@showNotebook' ); 


// ну есть шанс ещё, что в апаче не включен mod_rewrite


// Route::get('foo', function () {
//   return 'Hello World';
// }); 
Route::get('add', 'UserController1@addPeople');

Route::post('new', 'UserController1@newPeople'); 
// $name = Request::input('first_name', 'undefined'); 
// var_dump($name); 

Route::get('user/{word?}', function ($word) { 
  return 'Hello World - ' . $word;
})->name('profile'); 

Route::get('edit/{id}', 'UserController1@editView')->name('edit_teacher')->where('id', '[0-9]+'); 

Route::post('edit/{id}', 'UserController1@edit')->name('edit_post')->where('id', '[0-9]+'); 

Route::post('search', 'UserController1@search'); 