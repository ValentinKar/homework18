<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Homework18 extends Model
{
    //
	public $output = 'Homework18';  // 
	public $people; 

    public function work1()    // Вывод содержимого таблички notebook
    { 
    	/*
	    // $this->user = DB::table('user')->where( ['name' => 'Гамлет', 'email' => 'danmark'] )->first(); 
		$this->user = DB::table('user')
				->where( ['name' => 'Гамлет', 'email' => 'danmark'] )
				->value('id'); 

	    // $users1 = DB::select('select * from user where login = ?', ['admin']);
		$users1 = DB::table('user')->get(); 
		*/
		$teachers = DB::table('notebook')->get(); 
		    $this->people = []; 
			foreach ($teachers as $number => $teacher) { 
				foreach ($teacher as $key => $value) { 
					if ( $key !== 'created_at' && $key !== 'updated_at' )  { 
					$this->people[$number][$key] = $value; 
					};
				}; 
			}; 
	    return $this->people; 
    } 

    public function work2($new_teacher)     // добавление записи в табличку notebook
    { 
		$new = str_ireplace ( null , '' , $new_teacher ); 
		DB::table('notebook')->insert([	$new ]);
	    return true; 
    } 

    public function modelView($id)     // добавление записи в табличку notebook
    { 
		$teacher = []; 
		foreach (DB::table('notebook')->where(['id' => (integer)$id])->first() as $key => $value) { 
			if ( $key !== 'created_at' && $key !== 'updated_at' )  { 
			$teacher[$key] = $value; 
			};
		}; 
		return $teacher; 
    } 

    public function modelEdit($id, $edit_teacher)     // редактирование записи в табличке notebook
    { 
    	$edit = str_ireplace ( null , '' , $edit_teacher ); 
		DB::table('notebook')
        ->where('id', (integer)$id)
        ->update($edit);
        return true; 
    } 

    public function modelSearch($first, $last)      // поиск записи в табличке notebook
    { 
		$teachers = DB::table('notebook')
                //->where('first_name', 'like', '%нд%')
                ->where([
				['first_name', 'like', "%{$first}%"],
				['last_name', 'like', "%{$last}%"],
				])
                ->get(); 

		    $this->people = []; 
			foreach ($teachers as $number => $teacher) { 
				foreach ($teacher as $key => $value) { 
					if ( $key !== 'created_at' && $key !== 'updated_at' )  { 
					$this->people[$number][$key] = $value; 
					};
				}; 
			}; 
	    return $this->people; 
    }


}