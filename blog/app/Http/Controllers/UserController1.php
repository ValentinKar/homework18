<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use Illuminate\Routing\Controller; 
use App;

mb_internal_encoding('utf-8');  //позволяет использовать все ф-ции для кириллицы
error_reporting(E_ALL);        //вывести на экран все ошибки

class UserController1 extends Controller
{

    public function showNotebook() 
    { 
    	$homework_18 = new App\Homework18(); 
    	$teachers = $homework_18->work1(); 
        return view( $homework_18->output, [ 'value1' => 'Записная книжка', 'value2' => $teachers, 'value3' => '', 'value4' => '' ] ); 
    } 

    public function addPeople() 
    { 
        return view( 'Homework18_add' ); 
    } 

	public function newPeople(Request $request)
    // public function newPeople() 
    { 
    	// $name = Request::input('first_name', 'undefined'); 
		// var_dump($name); 
		$teacher = array_shift( $request->all() );
		array_shift($teacher); 
		// $name = $request->input('first_name');

    	$homework_18 = new App\Homework18(); 
    	if( $homework_18->work2($teacher) === true )  {  
    	$teachers = $homework_18->work1(); 
        return view( $homework_18->output, [ 'value1' => 'Записная книжка', 'value2' => $teachers, 'value3' => '', 'value4' => '' ] ); 
 		}; 

    } 

    public function editView($id) 
    { 
        $homework_18 = new App\Homework18(); 
        return view( 'Homework18_edit', [ 'teacher' => $homework_18->modelView($id) ] ); 
    }

    public function edit(Request $request, $id) 
    { 
        $teacher = $request->all();
        array_shift($teacher); 
        $homework_18 = new App\Homework18(); 
        if( $homework_18->modelEdit($id, $teacher) === true )  {  
        $teachers = $homework_18->work1(); 
        return view( $homework_18->output, [ 'value1' => 'Записная книжка', 'value2' => $teachers, 'value3' => '', 'value4' => '' ] ); 
        }; 
    }

    public function search(Request $request)
    {
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $first_name = isset($first_name) ? $first_name : ''; 
        $last_name = isset($last_name) ? $last_name : ''; 

    $homework_18 = new App\Homework18(); 
    $teachers = $homework_18->modelSearch($first_name, $last_name); 
    return view( $homework_18->output, [ 'value1' => 'Записная книжка', 'value2' => $teachers, 'value3' => $first_name, 'value4' => $last_name ] ); 
    }


} 