<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Записная книжка</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
/*            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 200vh;
                margin: 0;
            }

            .full-height {
                height: 200vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }


            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }*/
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">

            <h2>{{ $value1 }}</h2>

        <form method="POST" action="./search" enctype="">
        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
        {{ csrf_field() }}

        Найти 
        <label for="first_name">по имени: 
        <input type="text" name="first_name" id="first_name" value="{{ $value3 }}" placeholder="Иван">
        </label>

        <label for="last_name">по фамилии: 
        <input type="text" name="last_name" id="last_name" value="{{ $value4 }}" placeholder="Иванов">
        </label>

        <button type="submit">Найти </button> 
         <a href="{{ url('/') }}">Вывести на экран все записи</a>
        </form>

            <br />
            <a href="{{ url('/add') }}">Добавление в записную книжку</a>
            <br /><br />
            <a href="{{ route('profile') }}/beautiful">Пример именованного маршрута</a>

            <ol>
            @foreach($value2 as $value) 
                <li> {{ $value['first_name'] }} {{ $value['last_name'] }} 
                <a href="{{ route('edit_teacher', $value['id']) }}">Редактировать</a>
                    <ul>
                        @foreach($value as $k => $val)
                            <li>{{ $k }}: - {{ $val }}</li> 
                        @endforeach
                    </ul>
                </li>   
            @endforeach
            </ol>


            </div>
        </div>
    </body>
</html>
