<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Редактирование записи в записной книжке</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            /*html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }*/
            li {
                margin-bottom: 7px;
            }
        </style>
    </head>
    <body>

        <div class="content">

        <h2>Редактирование записи в записной книжке</h2>

        <form method="POST" action="{{ route('edit_post', $teacher['id']) }}" enctype="">
        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
        {{ csrf_field() }}

        <ol>
            <li><label for="first_name">Имя: <br /></label>
                <input type="text" name="first_name" id="first_name" value="{{ $teacher['first_name'] }}">
            </li>

            <li><label for="last_name">Фамилия: <br /></label>
                <input type="text" name="last_name" id="last_name" value="{{ $teacher['last_name'] }}">
            </li>

            <li><label for="mobil_phone">Сотовый телефон: <br /></label>
                <input type="text" name="mobil_phone" id="mobil_phone" value="{{ $teacher['mobil_phone'] }}">
            </li>

            <li>
                <label for="mobil_phone">Адрес: <br /></label>
                <textarea name="Adress" id="Adress" rows="3" cols="40">{{ $teacher['Adress'] }}</textarea>
            </li>

            <li><label for="Organization">Организация: <br /></label>
                <input type="text" name="Organization" id="Organization" value="{{ $teacher['Organization'] }}">
            </li>

            <li><label for="Job">Должность: <br /></label>
                <input type="text" name="Job" id="Job" value="{{ $teacher['Job'] }}">
            </li>

            <li>
                <label for="Comments">Комментарии: <br /></label>
                <textarea name="Comments" id="Comments" rows="3" cols="40">{{ $teacher['Comments'] }}</textarea>
            </li>

            <li><label for="Netology">Работа в Нетологии: <br /></label>
                <input type="text" name="Netology" id="Netology" value="{{ $teacher['Netology'] }}">
            </li>

            <li>
                <label for="Contacts">Контакты: <br /></label>
                <textarea name="Contacts" id="Contacts" rows="3" cols="40">{{ $teacher['Contacts'] }}</textarea>
            </li>

            <li><label for="Email">Электронная почта: <br /></label>
                <input type="text" name="Email" id="Email" value="{{ $teacher['Email'] }}">
            </li>

            <li><label for="vk">В контакте: <br /></label>
                <input type="text" name="vk" id="vk" value="{{ $teacher['vk'] }}">
            </li>

            <li><label for="fb">В фейсбуке: <br /></label>
                <input type="text" name="fb" id="fb" value="{{ $teacher['fb'] }}">
            </li>

        </ol>
        <button type="submit">Редактировать</button>
        </form>

        </div>

    </body>
</html>
